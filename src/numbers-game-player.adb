with Ada.Text_Io; use Ada.Text_Io;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with Ada.Unchecked_Deallocation ;

with cli ;

package body numbers.game.player is
   procedure free is new Ada.Unchecked_Deallocation(Digits_Type, DigitsHandle_Type);

   procedure EliminateCandidates( cand : in out Candidates_Type ; currGuess : Integer ; sc : Score_Type ) is
      csc : ScoreHandle_Type ;
      currGuessDigits : DigitsHandle_Type := Digitize(currGuess) ;
   begin
      for c in PotentialCandidates_Type
      loop
         if cand.handle(c) /= null
         then
            csc := Score(currGuessDigits,c) ;
            if csc.all /= sc
            then
               free( cand.handle(c) );
               cand.handle(c) := null ;
            end if ;
         end if ;
      end loop ;
   end EliminateCandidates ;

   procedure Play is
   begin
      loop
         declare
            cand : Candidates_Type := numbers.game.Candidates;
            myGuess : Integer ;
            score : Score_Type ;
         begin
            if cli.Verbose
            then
               Summarize(cand) ;
            end if ;

            loop
               myGuess := numbers.game.Choose(cand);
               Put("My guess is "); Put( myGuess ); put(" How did i do"); New_Line ;
               score.cows := Integer'Value(cli.Get("Cows"));
               score.bulls := Integer'Value(cli.Get("Bulls"));
               if score.cows = 4 and score.bulls = 0
               then
                  Put_Line("Wow. I did it.");
                  exit ;
               end if ;
               if score.cows = 0 and score.bulls = 0
               then
                  Put_Line("Too bad. Let me try again");
               else
                  EliminateCandidates( cand , myGuess , score ) ;
               end if ;
               if cli.Verbose
               then
                  Summarize(cand) ;
               end if ;
            end loop ;
            Put_Line("Next Game then");
         end ;
      end loop ;
    end Play ;
end numbers.game.player ;
