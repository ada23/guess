with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded ;
with gnat.strings ;

package cli is

   VERSION : string := "V01" ;
   NAME : String := "guess" ;
   Verbose : aliased boolean ;

   PlayerOption : aliased boolean ;
   HelpOption : aliased boolean ;

   procedure ProcessCommandLine ;
   function GetNextArgument return String ;
   procedure ShowCommandLineArguments ;

   function Get(prompt : string) return String ;
end cli ;
