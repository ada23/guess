with Ada.Text_Io; use Ada.Text_IO;
with Ada.Integer_Text_Io; use Ada.Integer_Text_Io ;

package body numbers.game is
   function Candidates return Candidates_Type is
      cands : Candidates_Type ;
   begin
      cands.handle := new CandidatesList_Type(PotentialCandidates_Type) ;

      for c in PotentialCandidates_Type'Range
      loop
         begin
            cands.handle(c) := Digitize(c,true) ;
         exception
            when others =>
               pragma Debug( Put_Line(Integer'Image(c) & " is not a candidate"));
         end ;
      end loop ;
      return cands ;
   end Candidates ;

   procedure Summarize( cand : Candidates_Type ) is
      total : integer := cand.handle'Length ;
      empty : integer := 0 ;
   begin
      for c in PotentialCandidates_Type
      loop
         if cand.handle(c) = null
         then
            empty := empty + 1 ;
         end if ;
      end loop ;
      Put("Total candidates "); Put(total); Put( " eliminated " ); Put(empty) ; New_Line ;
   end Summarize ;

   function Score( digs : DigitsHandle_Type ; guessed : Integer ) return ScoreHandle_Type is
      result : ScoreHandle_Type := new Score_Type ;
      guesseddigs : DigitsHandle_Type := Digitize(guessed) ;
   begin
      pragma Debug(Show(digs));
      pragma Debug(Show(guesseddigs));
      result.cows := 0 ;
      result.bulls := 0 ;
      if guesseddigs'length /= digs'length
      then
         raise Program_Error ;
      end if ;
      for d in digs.all'range
      loop
         if digs(d) = guesseddigs(d)
         then
            result.cows := result.cows + 1 ;
         end if ;
      end loop ;

      for d1 in digs.all'range
      loop
         for d2 in guesseddigs.all'range
         loop
            if d1 /= d2
            then
               if digs(d1) = guesseddigs(d2)
               then
                  result.bulls := result.bulls + 1 ;
               end if ;
            end if ;
         end loop ;
      end loop ;
      return result ;
   end Score ;

   function Score( cand : Candidates_Type ; toGuess : Integer; guessed : Integer ) return ScoreHandle_Type is
   begin
      return Score(cand.handle(toGuess), guessed) ;
   end Score ;


   Gen : Random_Pkg.Generator ;
   function Choose(pos : Candidates_Type) return PotentialCandidates_Type is
      cand : PotentialCandidates_Type ;
   begin
      loop
         cand := Random_Pkg.Random(Gen) ;
         if Pos.handle(cand) /= null
         then
            return cand ;
         end if ;
      end loop ;
   end Choose ;
begin
   Random_Pkg.Reset(Gen) ;
end numbers.game ;
