pragma Ada_2012;
with Ada.Text_Io; use Ada.Text_Io ;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;

with cli ;

package body numbers.game.dealer is

   ----------
   -- Play --
   ----------

   procedure Play is
      toGuess : integer ;
      cands : Candidates_Type := Candidates ;
   begin
      loop
         toGuess := Choose(cands);
         Put_Line("I have chosen a new number. Your Guesses please");
         Put_Line("A number or q for quit the game. I will show you my number");
         loop
            declare
               guess : string := cli.Get("Your Guess");
               yourscore : ScoreHandle_Type ;
            begin
               if guess(1..1) = "q"
               then
                  Put("I was thinking of "); Put(toGuess) ; New_line ;
                  return  ;
               end if ;
               yourscore := Score( cands , toGuess , Integer'Value(guess));
               if yourscore.cows = 4
               then
                  Put_Line("Congratulations. You guessed it");
                  exit;
               end if ;
               Put("Cows = "); Put(yourscore.cows); Put(" Bulls = "); Put(yourscore.bulls); New_line ;
            end ;
         end loop ;
      end loop ;
   end Play;

end numbers.game.dealer;
