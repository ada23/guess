with Ada.TExt_Io; use Ada.Text_Io ;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;

with cli ;
with numbers ; use numbers ;
with numbers.game ; use numbers.game ;
with numbers.game.dealer ; use numbers.game.dealer ;
with numbers.game.player ; use numbers.game.player ;

procedure Guess is
   procedure Numbers_Test is
      num1 : numbers.DigitsHandle_Type := numbers.Digitize( 10560522 ) ;
      val1 : Integer ;
   begin
      numbers.Show(num1);
      New_Line ;
      val1 := numbers.Value(num1) ;
      Put(val1);
      New_Line ;
   end Numbers_Test ;
   procedure Candidates_Test is
      cands : numbers.game.Candidates_Type  := numbers.game.Candidates ;
      numcands : integer := 0 ;
   begin
      for c in PotentialCandidates_Type
      loop
         if cands.handle(c) /= null
         then
            numbers.Show( cands.handle(c) );
            numcands := numcands + 1 ;
         end if ;
      end loop ;
      Put_Line(Integer'Image(numcands) & " candidates found");

      declare
         magic : DigitsHandle_Type := Digitize( 2345 ) ;
         score : numbers.Game.ScoreHandle_Type ;
      begin
         score := numbers.Game.Score( magic , 4523 );
         put( 4523 ) ;
         put( " cows " ); put(score.cows) ;
         put( " bulls " ); put(score.bulls) ;
         new_line ;
      end ;
   end Candidates_Test ;
   procedure Prompt_Test is
      cands : numbers.game.Candidates_Type  := numbers.game.Candidates ;
      LineNum : Integer := 0 ;
      NumToGuess : Integer ;
   begin
      loop
         lineNum := LineNum + 1 ;
         Put_Line(cli.Get("Prompt " & Integer'Image(LineNum)));
         NumToGuess := numbers.Game.Choose(cands) ;
         Put_Line("I am thinking of " & Integer'Image(NumToGuess));
      end loop ;
   end Prompt_Test;
   procedure DealerTest is
   begin
      numbers.game.dealer.Play ;
   end DealerTest ;
   procedure PlayerTest is
   begin
      numbers.game.player.Play ;
   end PlayerTest ;
begin
   cli.ProcessCommandLine;
   if cli.Verbose
   then
      cli.ShowCommandLineArguments;
   end if ;
   -- Numbers_Test ;
   -- Candidates_Test ;
   -- Prompt_Test ;
   -- DealerTest ;
   -- PlayerTest ;
   if cli.PlayerOption
   then
      numbers.game.Player.Play ;
   else
      numbers.game.dealer.Play ;
   end if ;
end Guess;
