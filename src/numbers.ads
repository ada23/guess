with Ada.Containers.Vectors ;

package numbers is

   type Digit_Type is new Integer range 0..9 ;
   type Digits_Type is array (Integer range <>) of Digit_Type ;
   type DigitsHandle_Type is access all Digits_Type ;
   procedure Show( Digs : Digits_Type ) ;
   procedure Show( DigsHandle : DigitsHandle_Type );

   type Integers_Type is array (Integer range <>) of Integer ;
   type IntegersHandle_Type is access all Integers_TYpe ;
   procedure Show( Integers : Integers_Type ) ;
   procedure Show( IntegersHandle : IntegersHandle_Type );

   function Digitize( Number : Integer ; unique : boolean := false ) return DigitsHandle_Type ;
   function Digitize( NumStr : string ; unique : boolean := false ) return DigitsHandle_Type ;
   function Value( digsHandle : DigitsHandle_Type ) return Integer ;
   function Image( digsHandle : DigitsHandle_Type ) return string ;

end numbers ;
