with Ada.Text_Io; use Ada.Text_Io ;

package body numbers is

   function Image( value : Integer ) return String is
      valstr : string := Integer'Image(value) ;
   begin
      for vidx in valstr'range
      loop
         if valstr(vidx) /= ' '
         then
            return valstr(vidx..valstr'Last) ;
         end if ;
      end loop ;
      raise Program_Error ;
   end Image ;

   procedure Show( Digs : Digits_Type ) is
   begin
      for d in Digs'range
      loop
         Put( Image(Integer(digs(d))) ) ; Put (" ") ;
      end loop ;
      New_Line ;
   end Show ;

   procedure Show( DigsHandle : DigitsHandle_Type ) is
   begin
      Show( digsHandle.all ) ;
   end Show ;

   procedure Show( Integers : Integers_Type ) is
   begin
      for int in Integers'range
      loop
         Put( Image(int) ) ; Put (" ") ;
      end loop ;
      New_Line ;
   end Show ;

   procedure Show( IntegersHandle : IntegersHandle_Type ) is
   begin
      Show( IntegersHandle.all ) ;
   end Show ;


   function Digitize( Number : Integer ; unique : boolean := false ) return DigitsHandle_Type is
      seendig : array (Digit_Type ) of boolean := (others => false) ;
      NumStr : string := Image(Number) ;
      digitsHandle : DigitsHandle_Type := new Digits_Type(NumStr'range) ;
      nextdig : Digit_Type ;
   begin
      for n in NumStr'Range
      loop
         nextdig := Digit_Type(Integer'Value( NumStr(n..n) )) ;
         if unique
         then
            if seendig(nextdig)
            then
               raise Program_Error ;
            end if ;
            seendig(nextdig) := true ;
            digitsHandle.all(n) := nextdig ;
         else
            digitsHandle.all(n) := nextdig ;
         end if ;
      end loop ;
      return digitsHandle ;
   end Digitize ;

   function Digitize( NumStr : string ; unique : boolean := false ) return DigitsHandle_Type is
      intval : Integer := Integer'Value(Numstr) ;
   begin
      return Digitize( intval , unique ) ;
   end Digitize ;

   function Value( digsHandle : DigitsHandle_Type ) return Integer is
       Val : Integer := 0 ;
    begin
       for D in digsHandle.all'Range
       loop
	  Val := Val * 10 + Integer(digsHandle.all(d)) ;
       end loop ;
       return Val ;
    end Value ;

   function Image( digsHandle : DigitsHandle_Type ) return string is
      val : Integer := Value(digsHandle) ;
   begin
      return Image(val) ;
   end Image ;

end numbers ;
