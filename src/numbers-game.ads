with Ada.Numerics.Discrete_Random ;

package numbers.game is

   subtype PotentialCandidates_Type is integer range 1023..9876 ;
   package Random_Pkg is new Ada.Numerics.Discrete_Random( PotentialCandidates_Type ) ;

   type CandidatesList_Type is array (integer range <>) of DigitsHandle_Type ;
   type Candidates_Type is
      record
         handle : access CandidatesList_Type ;
      end record ;

   -- type Candidates_Type is array (integer range <>) of DigitsHandle_Type ;
   function Candidates return Candidates_Type ;
   procedure Summarize( cand : Candidates_Type ) ;

   type Score_Type is
      record
         cows : integer := 0 ;
         bulls : integer := 0 ;
      end record ;
   type ScoreHandle_Type is access all Score_Type ;

   function Score( digs : DigitsHandle_Type ; guessed : Integer ) return ScoreHandle_Type ;
   function Score( cand : Candidates_Type ; toGuess : Integer ; guessed : Integer ) return ScoreHandle_Type ;

   function Choose (pos : Candidates_Type) return PotentialCandidates_Type ;

end numbers.game ;
